package io.piveau.scheduling.repositories;

import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GitRepository {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private String uri;
    private String branch;

    private String token;

    private String username;

    private Path localPath;

    public static GitRepository open(String uri, String username, String token, String branch) {
        if (uri != null) {
            if (branch != null && !branch.isEmpty()) {
                return new GitRepository(uri, username, token, branch);
            } else {
                return new GitRepository(uri, username, token, "master");
            }
        } else {
            return null;
        }
    }

    private GitRepository(String uri, String username, String token, String branch) {
        this.uri = uri;
        this.username = username;
        this.token = token;
        this.branch = branch;
        localPath = Paths.get("repositories").resolve(uri.replaceAll("[^\\w\\s]", "")).resolve(branch);
        if (!Files.exists(localPath)) {
            // Clone branch if not existent on disk
            log.info("cloning remote repo from " + uri + " to " + localPath.toString());
            cloneRepo();
        } else {
            pullRepo();
        }
    }

    public Path resolve(String path) {
        return localPath.resolve(path);
    }

    private void cloneRepo() {
        String finalUri = uri;

        if (token != null && !token.isEmpty()) {
            try {
                URL tmp = new URL(uri);
                finalUri = tmp.getProtocol() + "://" + username + ":" + URLEncoder.encode(token, "UTF-8") + "@" + tmp.getAuthority() + tmp.getFile();

            } catch (MalformedURLException e) {
                log.error("Parsing git repo uri", e);
            } catch (UnsupportedEncodingException e) {
                log.error("Encoding utf-8", e);
            }
        }

        CloneCommand cloneCommand = Git.cloneRepository()
                .setURI(finalUri)
                .setBranch(branch)
                .setDirectory(localPath.toFile())
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, token));

        try (Git git = cloneCommand.call()) {
            log.info("git clone successful");
        } catch (Exception e) {
            log.error("calling clone command", e);
        }
    }

    public void pullRepo() {
        try (Repository localRepo = new FileRepository(localPath + File.separator + ".git")) {
            try (Git repo = new Git(localRepo)) {
                PullCommand pullCommand = repo.pull();
                pullCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, token));

                PullResult pResult = pullCommand.call();
                MergeResult mResult = pResult.getMergeResult();

                if (!mResult.getMergeStatus().isSuccessful()) {
                    log.warn("could not merge repository: " + mResult.toString());
                }
            }
        } catch (IOException | GitAPIException e) {
            log.error("calling pull command", e);
        }
    }

}
