package io.piveau.scheduling.quartz;

import io.piveau.scheduling.repositories.GitRepository;
import io.vertx.core.Vertx;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.simpl.SimpleJobFactory;
import org.quartz.spi.TriggerFiredBundle;

import java.nio.file.Files;
import java.nio.file.Path;

public class VertxJobFactory extends SimpleJobFactory {

    private Vertx vertx;

    private GitRepository repository;

    public VertxJobFactory(Vertx vertx, String address, String branch, String username, String token) {
        this.vertx = vertx;
        repository = GitRepository.open(address, username, token, branch);
    }

    @Override
    public Job newJob(TriggerFiredBundle triggerFiredBundle, Scheduler scheduler) throws SchedulerException {
        final JobDetail jobDetail = triggerFiredBundle.getJobDetail();
        repository.pullRepo();
        Path path = repository.resolve("pipe-" + jobDetail.getKey().getName() + ".json");
        if (Files.exists(path)) {
            final Class<? extends Job> jobClass = jobDetail.getJobClass();
            try {
                return jobClass.getConstructor(Vertx.class, Path.class).newInstance(vertx, path);
            } catch (Exception e) {
                throw new SchedulerException("Could not create a job of type " + jobClass);
            }
        } else {
            throw new SchedulerException("No pipe for " + jobDetail.getKey().getName() + " available");
        }

    }

    public boolean pipeExists(String pipeId) {
        return Files.exists(repository.resolve("pipe-" + pipeId + ".json"));
    }

}
