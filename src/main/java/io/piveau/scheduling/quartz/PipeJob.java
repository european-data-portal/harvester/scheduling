package io.piveau.scheduling.quartz;

import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.pipe.model.Endpoint;
import io.piveau.pipe.model.Pipe;
import io.piveau.pipe.utils.PipeManager;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

@DisallowConcurrentExecution
public class PipeJob implements Job {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private WebClient client;
    private Path pipePath;

    public PipeJob(Vertx vertx, Path pipePath) {
        this.pipePath = pipePath;
        client = WebClient.create(vertx);
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        String triggerObject = jobExecutionContext.getMergedJobDataMap().getString("triggerObject");
        log.info("Job triggered: " + triggerObject);

        JsonObject trigger = new JsonObject(triggerObject);
        JsonObject configs = trigger.getJsonObject("configs");
        JsonObject config = trigger.getJsonObject("config");

        try {
            String pipeObject = new String(Files.readAllBytes(pipePath));
            Pipe pipe = Json.mapper.readValue(pipeObject, Pipe.class);
            if (config != null) {
                pipe.getBody().getSegments().forEach(segment -> {
                    JsonObject newConfig = config.copy();
                    newConfig.mergeIn(new JsonObject(segment.getBody().getConfig().toString()));
                    try {
                        segment.getBody().setConfig(Json.mapper.readTree(newConfig.toString()));
                    } catch (IOException e) {
                        log.error("Translate config json classes", e);
                    }
                });
            }
            if (configs != null) {
                pipe.getBody().getSegments().forEach(segment -> {
                    JsonObject segConfig = configs.getJsonObject(segment.getHeader().getName()).copy();
                    if (segConfig != null) {
                        segConfig.mergeIn(new JsonObject(segment.getBody().getConfig().toString()));
                        try {
                            segment.getBody().setConfig(Json.mapper.readTree(segConfig.toString()));
                        } catch (IOException e) {
                            log.error("Translate config json classes", e);
                        }
                    }
                });
            }
            if (jobExecutionContext.getMergedJobDataMap().containsKey("hubAddress")) {
                String hubAddress = jobExecutionContext.getMergedJobDataMap().getString("hubAddress");
                pipe.getBody().getSegments().forEach(segment -> {
                    if (segment.getHeader().getName().equals("exporting-hub")) {
                        ObjectNode exportConfig = (ObjectNode)segment.getBody().getConfig();
                        exportConfig.put("address", hubAddress);
                    }
                });
            }
            startPipe(pipe);
        } catch (IOException e) {
            log.error("read pipe", e);
        }

    }

    private void startPipe(Pipe pipe) {
        PipeManager pipeManager = PipeManager.create(pipe);
        Endpoint endpoint = pipeManager.getCurrentSegment().getBody().getEndpoint();

        Buffer buffer = Json.encodeToBuffer(pipeManager.getPipe());

        try {
            URL address = new URL(endpoint.getAddress());
            String method = endpoint.getMethod() != null ? endpoint.getMethod() : "POST";
            client.request(HttpMethod.valueOf(method), address.getPort(), address.getHost(), address.getPath()).putHeader("Content-Type", "application/json").sendBuffer(buffer, ar -> {
                if (ar.succeeded()) {
                    if (ar.result().statusCode() == 200 || ar.result().statusCode() == 202) {
                        log.debug("Pipe started");
                    }
                } else {
                    log.error("Send pipe", ar.cause());
                }
            });
        } catch (MalformedURLException e) {
            log.error("Address parsing", e);
        }
    }

}
