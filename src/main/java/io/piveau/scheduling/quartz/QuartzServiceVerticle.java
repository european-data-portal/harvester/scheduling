package io.piveau.scheduling.quartz;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.serviceproxy.ServiceBinder;
import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class QuartzServiceVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void start(Future<Void> startFuture) {

        initH2();

        String address = config().getString("PIVEAU_SCHEDULING_PIPE_REPOSITORY");
        String branch = config().getString("PIVEAU_SCHEDULING_PIPE_REPOSITORY_BRANCH", "develop");
        String username = config().getString("GITLAB_USERNAME");
        String token = config().getString("GITLAB_TOKEN");

        VertxJobFactory jobFactory = new VertxJobFactory(vertx, address, branch, username, token);

        String hubAddress = config().getString("PIVEAU_HUB_ADDRESS");

        QuartzService.create(hubAddress, jobFactory, ready -> {
            if (ready.succeeded()) {
                new ServiceBinder(vertx).setAddress(QuartzService.SERVICE_ADDRESS).register(QuartzService.class, ready.result());
                startFuture.complete();
            } else {
                startFuture.fail(ready.cause());
            }
        });
    }

    private void initH2() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:h2:file:./db/quartzdb", "sa", "");
            ResultSet rset = connection.getMetaData().getTables(null, null, "QRTZ_TRIGGERS", null);
            if (!rset.next()) {
                try(InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("tables_h2.sql")) {
                    RunScript.execute(connection, new InputStreamReader(inputStream));
                }
            }
            connection.close();
        } catch (Exception e) {
            log.error("Init H2 db", e);
        }
    }

}
