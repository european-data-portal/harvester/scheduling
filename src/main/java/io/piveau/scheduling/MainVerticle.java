package io.piveau.scheduling;

import io.piveau.scheduling.quartz.QuartzService;
import io.piveau.scheduling.quartz.QuartzServiceVerticle;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.cli.Argument;
import io.vertx.core.cli.CLI;
import io.vertx.core.cli.CommandLine;
import io.vertx.core.cli.Option;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.shell.ShellService;
import io.vertx.ext.shell.ShellServiceOptions;
import io.vertx.ext.shell.command.CommandBuilder;
import io.vertx.ext.shell.command.CommandProcess;
import io.vertx.ext.shell.command.CommandRegistry;
import io.vertx.ext.shell.term.HttpTermOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainVerticle extends AbstractVerticle {

    private QuartzService quartzService;

    @Override
    public void start(io.vertx.core.Future<Void> startFuture) {
        ConfigStoreOptions storeOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add("PIVEAU_SCHEDULING_PIPE_REPOSITORY")
                        .add("PIVEAU_SCHEDULING_PIPE_REPOSITORY_BRANCH")
                        .add("PIVEAU_HUB_ADDRESS")
                        .add("GITLAB_USERNAME")
                        .add("GITLAB_TOKEN")));

        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(storeOptions));
        Future<JsonObject> configFuture = ConfigRetriever.getConfigAsFuture(retriever);
        configFuture.compose(config -> {
            Future<String> verticleFuture = Future.future();
            vertx.deployVerticle(QuartzServiceVerticle.class, new DeploymentOptions().setWorker(true).setConfig(config), verticleFuture);
            return verticleFuture;
        }).compose(id -> {
            quartzService = QuartzService.createProxy(vertx, QuartzService.SERVICE_ADDRESS);

            Future<OpenAPI3RouterFactory> factoryFuture = Future.future();
            OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", factoryFuture);
            return factoryFuture;
        }).compose(routerFactory -> {
            RouterFactoryOptions options = new RouterFactoryOptions().setMountNotImplementedHandler(true).setMountValidationFailureHandler(true);
            routerFactory.setOptions(options);
            routerFactory.addHandlerByOperationId("listTriggers", this::handleListTriggers);
            routerFactory.addHandlerByOperationId("getTriggers", this::handleGetTriggers);
            routerFactory.addHandlerByOperationId("createOrUpdateTriggers", this::handleCreateOrUpdateTriggers);
            routerFactory.addHandlerByOperationId("deleteTriggers", this::handleDeleteTriggers);
            routerFactory.addHandlerByOperationId("setTriggerStatus", this::handleSetTriggerStatus);
            routerFactory.addHandlerByOperationId("bulkUpdate", this::handleBulkUpdate);

            Router router = routerFactory.getRouter();
            router.route().order(0).handler(CorsHandler.create("*").allowedHeader("Content-Type").allowedMethods(Stream.of(HttpMethod.PUT, HttpMethod.GET).collect(Collectors.toSet())));

            router.route("/*").handler(StaticHandler.create());

            ShellService shellService = ShellService.create(vertx, new ShellServiceOptions()
                    .setWelcomeMessage("\n piveau scheduling shell\n\n")
                    .setHttpOptions(new HttpTermOptions().setHost("localhost").setPort(8085)));
            shellService.start();

            CLI cli = CLI.create("trigger-ls").
                    addArgument(new Argument().setRequired(false).setArgName("id")).
                    addOption(new Option().setFlag(true).setShortName("s").setLongName("short"));
            CommandBuilder command = CommandBuilder.command(cli);
            command.processHandler(this::handleTriggerLs);
            CommandRegistry registry = CommandRegistry.getShared(vertx);
            registry.registerCommand(command.build(vertx));

            HealthCheckHandler hch = HealthCheckHandler.create(vertx);
            hch.register("buildInfo", future -> vertx.fileSystem().readFile("buildInfo.json", bi -> {
                if (bi.succeeded()) {
                    future.complete(Status.OK(bi.result().toJsonObject()));
                } else {
                    future.fail(bi.cause());
                }
            }));
            router.get("/health").handler(hch);

            HttpServer server = vertx.createHttpServer(new HttpServerOptions().setPort(8080));
            server.requestHandler(router).listen();

            startFuture.complete();

        }, startFuture);
    }

    private void handleListTriggers(RoutingContext routingContext) {
        quartzService.listTriggers(ar -> {
            if (ar.succeeded()) {
                routingContext.response().end(ar.result().encodePrettily());
            } else {
                routingContext.response().setStatusCode(500).end();
            }
        });
    }

    private void handleBulkUpdate(RoutingContext routingContext) {
        JsonObject bulk = routingContext.getBodyAsJson();

        ArrayList<Future> futureList = new ArrayList<>();

        bulk.fieldNames().forEach(name -> {
            JsonArray triggers = bulk.getJsonArray(name);
            Future<String> future = Future.future();
            quartzService.createOrUpdateTrigger(name, triggers, future);
            futureList.add(future);
        });
        CompositeFuture.join(futureList).setHandler(ar -> {
            if (ar.succeeded()) {
                routingContext.response().end();
            } else {
                routingContext.response().setStatusCode(200).setStatusMessage("Not all triggers were successfully created or updated.").end();
            }
        });
    }

    private void handleGetTriggers(RoutingContext routingContext) {
        String pipeId = routingContext.pathParam("pipeId");
        quartzService.getTriggers(pipeId, ar -> {
            if (ar.succeeded()) {
                routingContext.response().end(ar.result().encodePrettily());
            } else {
                routingContext.response().setStatusCode(404).putHeader("Content-Type", "text/plain").end(ar.cause().getMessage());
            }
        });
    }

    private void handleCreateOrUpdateTriggers(RoutingContext routingContext) {
        String pipeId = routingContext.pathParam("pipeId");
        JsonArray triggers = routingContext.getBodyAsJsonArray();
        quartzService.createOrUpdateTrigger(pipeId, triggers, ar -> {
            if (ar.succeeded()) {
                int code = "created".equalsIgnoreCase(ar.result()) ? 201 : 200;
                routingContext.response().setStatusCode(code).end();
            } else {
                routingContext.response().setStatusCode(500).end();
            }
        });
    }

    private void handleDeleteTriggers(RoutingContext routingContext) {
        String pipeId = routingContext.pathParam("pipeId");
        quartzService.deleteTriggers(pipeId, ar -> {
            if (ar.succeeded()) {
                routingContext.response().end();
            } else {
                routingContext.response().setStatusCode(404).end();
            }
        });
    }

    private void handleSetTriggerStatus(RoutingContext routingContext) {
        String pipeId = routingContext.pathParam("pipeId");
        String triggerId = routingContext.pathParam("triggerId");
        String status = routingContext.pathParam("status");

        quartzService.setTriggerStatus(pipeId, triggerId, status, ar -> {
            if (ar.succeeded()) {
                routingContext.response().end(ar.result());
            } else {
                String cause = ar.cause().getMessage();
                if (cause.contains("not found")) {
                    routingContext.response().setStatusCode(404).end(cause);
                } else if (cause.equals("status already set or unknown")){
                    routingContext.response().setStatusCode(409).end(cause);
                } else {
                    routingContext.response().setStatusCode(500).end(cause);
                }
            }
        });

    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

    private void handleTriggerLs(CommandProcess process) {
        CommandLine commandLine = process.commandLine();
        String id = commandLine.getArgumentValue("id");
        boolean shortFlag = commandLine.isFlagEnabled("s");

        process.write("Works!\n");
        process.end();
    }

}
