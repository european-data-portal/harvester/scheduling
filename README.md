# scheduling
Microservice for scheduling pipes.

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Interface](#interface)
1. [Docker](#docker)
1. [License](#license)

## Build
Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/harvester/scheduling.git
$ cd scheduling
$ mvn package
```

## Run

```bash
$ java -jar target/scheduling-far.jar
```

## Interface

The documentation of the REST service can be found when the root context is opened in a browser.

```
http://localhost:8080/
```

## Docker

Build docker image:
```bash
$ docker build -t scheduling .
```

Run docker image:
```^bash
$ docker run -it -p 8080:8080 scheduling
```

## License

[Apache License, Version 2.0](LICENSE.md)
